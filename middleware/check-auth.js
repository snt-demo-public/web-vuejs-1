export default ({ redirect }) => {
  const token = localStorage.getItem('access-token');
  if (token) {
    redirect('/')
    return;
  }
}
