export const state = () => ({
  files: [],
  uploadedFiles: [],
  isUploadSuccess: false,
})

export const mutations = {
  onGetFiles: (state, data) => {
    state.files = data;
  },
  onGetUploadedFiles: (state, data) => {
    state.uploadedFiles.push(data);
  },
  onRemoveFile: (state, id) => {
    state.uploadedFiles = state.uploadedFiles.filter(item => item._id !== id);
  },
  onSetUploadSuccess: (state, isUploadSuccess) => {
    state.isUploadSuccess = isUploadSuccess;
  },
  onResetUploadedFile: (state) => {
    state.uploadedFiles = [];
  }
}
